#!/usr/bin/env php
<?php
/**
 * FAO SMTA submitter
 * Copyright (C) 2018 Endless-Dream, Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use endlessdreams\easysmta\action\DryRun;
use Zend\Console\Console;
use ZF\Console\Application;
use ZF\Console\Dispatcher;

require_once __DIR__ . '/../../../vendor/autoload.php'; // Composer autoloader

define('VERSION', '0.1.9');

$dispatcher = new Dispatcher();
$dispatcher->map('dry-run', new DryRun());


$application = new Application(
    'easysmta',
    VERSION,
    include __DIR__ . '/config/routes.php',
    Console::getInstance(),
    $dispatcher
    );
$application->setBanner(function ($console) {           // callable
    $console->writeLine(
        $console->colorize('easysmta', \Zend\Console\ColorInterface::BLUE)
        . ' - for exporting easy smta xml data set to FAO'
        );
    $console->writeLine('');
    $console->writeLine('Usage:', \Zend\Console\ColorInterface::GREEN);
    $console->writeLine(' ' . basename(__FILE__) . ' command [options]');
    $console->writeLine('');
    $console->writeLine('Copyright 2018 Endless-Dreams');
});

$application->setBannerDisabledForUserCommands(true);
$application->setFooter(null);
$exit = $application->run();
exit($exit);
