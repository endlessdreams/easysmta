<?php
// config/routes.php

return [
    [
        'name' => 'dry-run',
        'route' => '[--table=] [--from=] [--to=]',
        'description' => 'Dry run using db config file to connect to database. If no table name is assigned use map config file',
        'short_description' => 'Dry run',
        'options_descriptions' => [
            '--table' => 'Name of table och dataview to fetch and export as xml',
            '--from' => 'From which SMTA date to start report',
            '--to' => 'To which SMTA date to end report'
        ],
        'defaults' => [
            'table' => null, // Use map config file
            'from' => null, // Start with first record
            'to' => null, // End with last record
        ],
        'handler' => 'endlessdreams\easysmta\action\DryRun'
    ],
    [
        'name' => 'post',
        'route' => '[--table=] [--from=] [--to=] [-t]',
        'description' => 'Post SMTAs using db config file to connect to database. If no table name is assigned use map config file',
        'short_description' => 'Post SMTAs',
        'options_descriptions' => [
            '--table' => 'Name of table och dataview to fetch and export as xml',
            '--from' => 'From which SMTA date to start report',
            '--to' => 'To which SMTA date to end report',
            '-t' => 'Test'
        ],
        'defaults' => [
            'table' => null, // Use map config file
            'from' => null, // Start with first record
            'to' => null, // End with last record
            't' => false, // No default testing
        ],
        'handler' => 'endlessdreams\easysmta\action\Post'
    ]
];