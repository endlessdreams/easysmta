<?php
return [
    'database' => [
        'driver' => getenv('EASYSMTA_DRIVER'),
        'database' => getenv('EASYSMTA_DATABASE'),
        'username' => getenv('EASYSMTA_USERNAME'),
        'password' => getenv('EASYSMTA_PASSWORD'),
        'hostname'  => getenv('EASYSMTA_HOSTNAME'),
    ],
    'map' => [
        'table_order' => 'easysmta',
        'table_item' => 'easysmtaitem',
        'columns_order' => [
            'id' => 'id',
            'symbole' => 'symbole',
            'date' => 'date',
            'type' => 'type',
            'language' => 'language',
            'shipName' => 'shipname',
            'recipient_type' => 'recipient_type',
            'recipient_pid' => 'recipient_pid',
            'recipient_name' => 'recipient_name',
            'recipient_address' => 'recipient_address',
            'recipient_country' => 'recipient_country',
            'document_location' => 'document_location',
            'document_retInfo' => 'document_retinfo'
        ],
        'columns_item' => [
            'crop' => 'crop',
            'sampleid' => 'sampleid',
            'pud' => 'pud',
            'ancestry' => 'ancestry'
        ],
    ],
    'provider' => [
        'type' => 'or',
        'pid' => 'YOUR INSTITUTE CODE',
        'name' => 'YOUR INSTITUTE NAME',
        'address' => 'YOUR INSTITUTE ADDRESS',
        'country' => 'XXX',
        'email' => 'contact@any.institute'
    ],
    'fao' => [
        'username' => getenv('EASYSMTA_FAO_USERNAME'),
        'password' => getenv('EASYSMTA_FAO_PASSWORD'),
    ],
];