<?php
/**
 * FAO SMTA submitter
 * Copyright (C) 2018 Endless-Dream, Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace endlessdreams\easysmta\helper;


use endlessdreams\easysmta\overrides\Xml;
use Zend\Config\Config;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db;


class XmlGenerator {
    
    /** @var Zend\Db\Adapter\Adapter */
    public $db;
    
    /** @var array */
    public $row;
    
    /** @var array */
    public $dbMap;
    
    /** @var array */
    public $provider;
    
    /**
     * @param \Zend\Db\Adapter\Adapter $db
     * @param array $row
     * @param array $dbMap
     * @param array $provider
     */
    function __construct(\Zend\Db\Adapter\Adapter $db, array $row = null, array $dbMap = null, array $provider = null) {
        $this->db = $db;
        $config = new Config(include __DIR__.'/../../config/config.php');
        $this->dbMap = $dbMap ?? $config->map->toArray();
        $this->provider = $provider ?? $config->provider->toArray();
    }
    
    
    /**
     * @throws \Exception
     * @return string
     */
    function __invoke() {
        $smta = new \Zend\Config\Config(array(), true);
        $symbole = $this->row[$this->dbMap['columns_order']['symbole']];
        $smta->symbol = $symbole;
        $smta->date = $this->row[$this->dbMap['columns_order']['date']];
        $type = $this->row[$this->dbMap['columns_order']['type']];
        $smta->type = $type;
        
        $smta->language = $this->row[$this->dbMap['columns_order']['language']];
        
        $shipName = $this->row[$this->dbMap['columns_order']['shipName']];
        if (empty($ancestry) && $type == 'sw') {
            throw new \Exception("SMTA {$symbole} has a validation error: A shipName is required when SMTA is an shrink-wrap.");
        }
        $smta->provider = $this->provider;
        
        $smta->recipient = [];
        $smta->recipient->type = $this->row[$this->dbMap['columns_order']['recipient_type']];
        $smta->recipient->name = $this->row[$this->dbMap['columns_order']['recipient_name']];
        $smta->recipient->address = $this->row[$this->dbMap['columns_order']['recipient_address']];
        $smta->recipient->country = $this->row[$this->dbMap['columns_order']['recipient_country']];
        
        $smta->annex1 = [];
        $smta->annex1->material = [];
        
        $sql = "SELECT * FROM " . $this->dbMap['table_item'] . " where orderid = ". $this->row[$this->dbMap['columns_order']['id']];
        $statement = $this->db->query($sql);
        
        /** @var $results Zend\Db\ResultSet\ResultSet */
        $results = $statement -> execute();
        foreach ($results as $r) {
            
            $material = [
                'crop' => $r[$this->dbMap['columns_item']['crop']],
                'sampleID' => $r[$this->dbMap['columns_item']['sampleid']],
                'pud' => $r[$this->dbMap['columns_item']['pud']]
            ];
            
            $ancestry = $r[$this->dbMap['columns_item']['ancestry']] ?? "";
            
            if (!empty($ancestry) && $material['pud'] == 'y') {
                $material['ancestry'] = $ancestry;
            }
            if (empty($ancestry) && $material['pud'] == 'y') {
                throw new \Exception("SMTA {$symbole} has a validation error: A material pud=y ancestry is required.");
            }
            
            $smta->annex1->material[] = $material;
        }
        
        $smta->document = [];
        $documentLocation = $this->row[$this->dbMap['columns_order']['document_location']];
        $smta->document->location = $documentLocation;
        $documentRetInfo = $this->row[$this->dbMap['columns_order']['document_retInfo']];
        if (!empty($documentLocation) && $documentLocation == 's' && !empty($documentRetInfo)) {
            throw new \Exception("SMTA {$symbole} has a validation error: A document location s shall not have a retInfo.");
        }
        $smta->document->retInfo = $documentRetInfo;
        
        $writer = new Xml();
        $writer->setStartElement('smta');
        
        return $writer->toString($smta);
    }
}