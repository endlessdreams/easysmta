<?php
/**
 * FAO SMTA submitter
 * Copyright (C) 2018 Endless-Dream, Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace endlessdreams\easysmta\overrides;

use XMLWriter;
use Zend\Config\Exception;

/**
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 */
class Xml extends \Zend\Config\Writer\Xml
{
    
    /** @var string */
    protected $_startElement = "zend-config";
    
    /**
     * processConfig(): defined by AbstractWriter.
     *
     * @param  array $config
     * @return string
     */
    public function processConfig(array $config)
    {
        $writer = new XMLWriter();
        $writer->openMemory();
        $writer->setIndent(true);
        $writer->setIndentString(str_repeat(' ', 4));
        
        $writer->startDocument('1.0', 'UTF-8');
        if(isset($this->_startElement)) {
            $writer->startElement($this->_startElement);
        }
        
        
        foreach ($config as $sectionName => $data) {
            if (!is_array($data)) {
                $writer->writeElement($sectionName, (string) $data);
            } else {
                $this->addBranch($sectionName, $data, $writer);
            }
        }
        
        if(isset($this->_startElement)) {
            $writer->endElement();
        }
        $writer->endDocument();
        
        return $writer->outputMemory();
    }
    
    
    
    
    /**
     * @param string $value
     */
    public function setStartElement($value = null) {
        $this->_startElement = $value;
    }
}