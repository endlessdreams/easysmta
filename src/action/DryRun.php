<?php
/**
 * FAO SMTA submitter
 * Copyright (C) 2018 Endless-Dream, Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace endlessdreams\easysmta\action;

use Zend\Config\Config;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db;
use endlessdreams\easysmta\helper\XmlGenerator;




/**
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 */
class DryRun {
    
    /** @var Zend\Db\Adapter\Adapter */
    protected $db;
    
    /** @var array */
    protected $dbMap;
    
    /** @var array */
    protected $provider;
    
    /**
     * 
     */
    public function __construct() {
        $config = new Config(include __DIR__.'/../../config/config.php');
        $this->db = new Adapter($config->database->toArray());
        
        $this->dbMap = $config->map->toArray();
        $this->provider = $config->provider->toArray();
    } 
    
    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     */
    public function __invoke(\ZF\Console\Route $route, \Zend\Console\Adapter\AdapterInterface $console)
    {
        $xml = new XmlGenerator($this->db);
        
        foreach ($this->getSmtaRecords($route) as $r) {
            $xml->row = $r;
            $console->writeLine( $xml() );
        }
         
    }
    
    /**
     * @param \ZF\Console\Route $route
     * @return \endlessdreams\easysmta\action\Zend\Db\ResultSet\ResultSet
     */
    protected function getSmtaRecords(\ZF\Console\Route $route) {
        $from = $route->getMatchedParam('from');
        $to = $route->getMatchedParam('to');
        
        $sql = "SELECT * FROM " . $this->dbMap['table_order'] . " where true";
        if(isset($from)) {
            $sql .= " and date > '$from'";
        }
        
        if(isset($to)) {
            $sql .= " and date < '$to'";
        }
        
        $statement = $this->db->query($sql);
        
        /** @var $results Zend\Db\ResultSet\ResultSet */
        $results = $statement -> execute();
        
        return $results;
    }
}