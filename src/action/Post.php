<?php
/**
 * FAO SMTA submitter
 * Copyright (C) 2018 Endless-Dream, Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace endlessdreams\easysmta\action;

use Zend\Config\Config;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\Db;
use endlessdreams\easysmta\helper\XmlGenerator;
use Curl\Curl;



/**
 * @author Kjell-Åke Lundblad <kjellake.lundblad@endlessdreams.biz>
 * @since v0.0.1
 */
class Post extends DryRun {
    

    /**
     *
     */
    public function __construct() {
        parent::__construct();
        
        $config = new Config(include __DIR__.'/../../config/config.php');
        $this->fao = $config->fao->toArray();
        
    } 
    
    /**
     * @param \ZF\Console\Route $route
     * @param \Zend\Console\Adapter\AdapterInterface $console
     */
    public function __invoke(\ZF\Console\Route $route, \Zend\Console\Adapter\AdapterInterface $console)
    {
        try {
            $xml = new XmlGenerator($this->db);
            $t = $route->getMatchedParam('t');
            foreach ($this->getSmtaRecords($route) as $r) {
                $xml->row = $r;
                
                $post_data = array(
                    'username' => $this->fao['username'],
                    'password' => $this->fao['password'],
                    'compressed' => 'n',
                    'xml'=>$xml()
                );
                if ($t == true) {
                    $url = "https://easy-smta-test.planttreaty.org/itt/index.php?r=extsys/uploadxml";
                } else {
                    $url = "https://easy-smta-test.planttreaty.org/itt/index.php?r=extsys/uploadxml";
                }
                $port=443;
                
                $console->writeLine($this->_post($post_data,$url,$port));
                
            }
        } catch (\Exception $e) {
            $console->writeLine("SMTA {$r['symbole']}: $e");
        }
    }
    
    
    
    
    /**
     * @param array $post_xml
     * @param string $url
     * @param unknown $port
     * @throws \Exception
     * @return string
     */
    protected function _post($post_xml,$url,$port) {
        /** @var Curl\Curl */
        $curl = new \Curl\Curl();
                
        $curl->setOpt(CURLOPT_FAILONERROR, TRUE); // Fail on errors
        $curl->setOpt(CURLOPT_RETURNTRANSFER, TRUE); // return into a variable
        $curl->setOpt(CURLOPT_PORT, $port); //Set the port number
        $curl->setOpt(CURLOPT_PORT, $port); // times out after 30 minutes
        $curl->setOpt(CURLOPT_HTTPHEADER, array('Connection: close'));
        $curl->setOpt(CURLOPT_HEADER, TRUE); // Show header in reply

        
        if($port==443) {
            //curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
            $curl->setOpt(CURLOPT_SSL_VERIFYHOST, 2);
            //curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,1);
            $curl->setOpt(CURLOPT_SSL_VERIFYPEER, 1);
        }
        
        $curl->post($url, $post_xml);
        
        if ($curl->isError()) {
            // return 'ERROR -> '.curl_errno($ch).': '.curl_error($ch);
            return 'ERROR -> '.$curl->curl_error_code.': '.$curl->curl_error_message;
        } else {
            switch($curl->http_status_code){
                case 200:
                    break;
                default:
                    throw new \Exception('HTTP ERROR -> '.$curl->http_status_code);
                    break;
            }
        }
        $data = $curl->response;
        $curl->close();
        
        return $data;
    }

}